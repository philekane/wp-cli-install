#!/bin/bash

read -p "App Name: " APP_NAME
read -p "URL: " URL
read -p "Admin email: " ADMIN_EMAIL
read -p "Path to your project: (e.g., /var/www) " PATH

cat > /etc/nginx/sites-available/$APP_NAME << EOF

fastcgi_cache_path $PATH/$APP_NAME/cache levels=1:2 keys_zone=$APP_NAME:100m inactive=60m;

# Expires map
map \$sent_http_content_type \$expires {
    default                    off;
    text/html                  epoch;
    text/css                   max;
    text/js                    max;
    application/js             max;
    application/javascript     max;
    ~image/                    max;
}
server {
    listen 80;
    listen [::]:80;

    expires \$expires;

    server_name $APP_NAME  www.$APP_NAME;

    access_log $PATH/$APP_NAME/logs/access.log;
	error_log $PATH/$APP_NAME/logs/error.log;
    root $PATH/$APP_NAME/public/;
    index index.php;

    set \$skip_cache 0;


        # POST requests and urls with a query string should always go to PHP
        if (\$request_method = POST) {
        set \$skip_cache 1;
        }
        if (\$query_string != "") {
        set \$skip_cache 1;
        }

        # Don’t cache uris containing the following segments
        if (\$request_uri ~* "/wp-admin/|/xmlrpc.php|wp-.*.php|/feed/|index.php|sitemap(_index)?.xml") {
            set \$skip_cache 1;
        }

        # Don’t use the cache for logged in users or recent commenters
        if (\$http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass|wordpress_no_cache|wordpress_logged_in") {
            set \$skip_cache 1;
        }

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }
	
   listen 443 ssl http2;
   listen [::]:443 ssl http2;

   #ssl_certificate /etc/letsencrypt/live/$APP_NAME/fullchain.pem;
   #ssl_certificate_key /etc/letsencrypt/live/$APP_NAME/privkey.pem;

    location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;

        fastcgi_cache_bypass \$skip_cache;
        fastcgi_no_cache \$skip_cache;
        fastcgi_cache $APP_NAME;
        fastcgi_cache_valid 60m;

    }
}
EOF
ln -s /etc/nginx/sites-available/$APP_NAME /etc/nginx/sites-enabled/$APP_NAME
chown root:root /etc/nginx/sites-available/$APP_NAME

#Clean up files:
cd $PATH/$APP_NAME/public/

rm -f wp-config-sample.php
rm -f readme.html
rm -f licence.txt

find . -type d -exec sudo chmod 750 {} \;
find . -type f -exec sudo chmod 640 {} \;
chmod 400 wp-config.php

cd $PATH/$APP_NAME/public/wp-admin
rm -f install.php

echo "Don't forget to add $URL to your hosts file or DNS. Test nginx config
sudo nginx -t and if its ok then run letsencrypt "sudo certbot --nginx certonly" then "sudo service nginx restart"


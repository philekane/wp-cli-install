#!/bin/bash

read -p "App Name: " APP_NAME
read -p "URL: " URL
read -p "Admin email: " ADMIN_EMAIL
read -p "Path to your project: (e.g., /var/www) " PATH

cat > /etc/httpd/conf.d/$APP_NAME.conf << EOF
<VirtualHost *:80>
        ServerName $URL
	ServerAlias $URL
	ServerAdmin $ADMIN_EMAIL
	DocumentRoot $PATH/$APP_NAME

	<Directory $PATH/$APP_NAME>
	     Options FollowSymLinks
	     AllowOverride All
	     Order allow,deny
	     Allow from all
	    Require all granted
	</Directory>
</VirtualHost>
EOF

chown root:root /etc/httpd/conf.d/$APP_NAME.conf
#service httpd restart

cp ~/wp-cli-install/.htaccess $PATH/$APP_NAME/public/

#clean up files:
cd $PATH/$APP_NAME/public
rm -f wp-config-sample.php
rm -f readme.html
rm -f licence.txt
find . -type d -exec sudo chmod 750 {} \;
find . -type f -exec sudo chmod 640 {} \;
chmod 400 wp-config.php
cd $PATH/$APP_NAME/public/wp-admin
rm -f install.php

echo "Test your httpd config with: sudo apachectl -t and then restart if OK. Don't forget to add $URL to your hosts file or DNS"
								    


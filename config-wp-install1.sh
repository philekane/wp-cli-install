#!/bin/bash

read -p "App Name: " APP_NAME
read -p "Database Name(do not use .com): " DB_NAME
read -p "Database-user: " DB_USER
read -s -p "Database Password: " PASSWORD
read -p "Prefix - use only letters and numbers: " PREFIX
read -p "Admin email: " ADMIN_EMAIL
read -p "Path to your project: (e.g., /var/www) " PATH
echo

mkdir -p $PATH/$APP_NAME $PATH/$APP_NAME/logs $PATH/$APP_NAME/public $PATH/$APP_NAME/cache $PATH/$APP_NAME/scripts $PATH/$APP_NAME/repo
chmod -R 750 $APP_NAME
wp core download --path="$PATH/$APP_NAME/public/"
cd $PATH/$APP_NAME/public/
read  -p "Mysql Root Password: " ROOT_PASSWORD
sudo mysql -u root -p "$ROOT_PASSWORD" <<SQL
CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$PASSWORD';
CREATE DATABASE $DB_NAME;
GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$DB_USER'@'localhost';
SQL

wp config create --path="$PATH/$APP_NAME/public/" --dbprefix="wp_"$PREFIX"_" --dbname="$DB_NAME" --dbuser="$DB_USER" --dbpass="$PASSWORD" --extra-php <<PHP
/*disable file editing in the admin dashboard of themes and plugins*/
define('AUTOMATIC_UPDATER_DISABLED', true ); // yes, it's safe to do it manually
define('DISALLOW_FILE_EDIT', true);
define('DISALLOW_FILE_MODS', true ); // yes, it's safe to do it manually
define('FS_METHOD', 'direct'); // no FTP of course
define('WP_HTTP_BLOCK_EXTERNAL', true );
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );
define('WP_DEBUG_DISPLAY',false);
ini_set('display_errors', off);
PHP

read -p "Admin email: " ADMIN_EMAIL
read -p "Admin user: " ADMIN_USER
read -p "Url: " URL
read -p "Website title: " TITLE
read -s -p "Admin password: " ADMIN_PWD
read -p "Display/Nickname: (full name?) " NAME
read -p "Is this a multisite install - yes/no: " MULTI_SITE

if [ $MULTI_SITE == "yes" ]; then
        wp core multisite-install --url="$URL" --title="$TITLE" --admin_user="$ADMIN_USER" --admin_password="$ADMIN_PWD" --admin_email="$ADMIN_EMAIL"
else
        wp core install --url="$URL" --title="$TITLE" --admin_user="$ADMIN_USER" --admin_password="$ADMIN_PWD" --admin_email="$ADMIN_EMAIL"
fi

# change example to new domain
DOMAIN =  "${$URL%%.*}"
sed -i 's/example/'$DOMAIN'/g'  $PATH/$APP_NAME/public/.htacess

#update admin-user nickname so username is not shown in site-map
wp user update 1 --display_name=$NAME --nickname=$NAME

# add plugins that you normally install and uncomment following line
# ~/wp-cli-install/plugins/bizsitesetc_misc_code.zip
# ~/wp-cli-install/plugins/biz_sites_etc_email_transport.zip 
#~/wp-cli-install/plugins/biz_sites_etc_cookie_notification.zip

wp plugin install --activate nginx-cache wp-cerber wordpress-seo wp-smushit updraftplus
 ~/wp-cli-install/plugins/bizsitesetc_misc_code.zip ~/wp-cli-install/plugins/biz_sites_etc_email_transport.zip ~/wp-cli-install/plugins/biz_sites_etc_cookie_notification.zip

#add welcome theme
wp theme install --activate ~/wp-cli-install/themes/bizsitesetc-coming-soon.zip

# add content to the original post
#wp @dev post update 1 --post_name='"$TITLE" - Website Coming Soon' --post_title='"$TITLE"' --post_status='publish' ~/$APP_NAME/welcome.txt


cp -R . $PATH/$APP_NAME/repo/

cd $PATH/$APP_NAME/repo/
git init
git add --all
git commit -m "initial commit"

echo "run sudo bash ~/wp-cli-install/config-wp-install2.sh to add the virtual server $APP_NAME file in /nginx/sites-available/"
echo "create repository on gitbucket then push to it"

#!/bin/bash

read -p "App Name: " APP_NAME
read -p "URL: " URL
read -p "Admin email: " ADMIN_EMAIL
read -p "Path to your project: (e.g., /var/www) " PATH

cat > /etc/nginx/sites-available/$APP_NAME << EOF

server {
    listen 80;
    listen [::]:80;

    server_name $APP_NAME  www.$APP_NAME;

    access_log $PATH/$APP_NAME/logs/access.log;
    error_log $PATH/$APP_NAME/logs/error.log;

    root $PATH/$APP_NAME/public/;
    index index.php;

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
	    }

    location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }
}
EOF
ln -s /etc/nginx/sites-available/$APP_NAME /etc/nginx/sites-enabled/$APP_NAME
chown root:root /etc/nginx/sites-available/$APP_NAME
chmod 755 /etc/nginx/sites-available/$APP_NAME

#clean up files:
cd $PATH/$APP_NAME/public/
rm -f wp-config-sample.php
rm -f readme.html
rm -f licence.txt
find . -type d -exec sudo chmod 750 {} \;
find . -type f -exec sudo chmod 640 {} \;
chmod 400 wp-config.php
cd $PATH/$APP_NAME/public/wp-admin
rm -f install.php

echo "Test the config with: "sudo nqinx -t" if OK restart with "sudo service nginx restart" .Don't forget to add $URL to your hosts file or DNS."
										    

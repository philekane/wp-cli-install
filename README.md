# wp-cli-install


Bash files for setting up a WordPress install with .htaccess file that increases page speed and makes website more secure. 

Install wordpress with wp-cli.

You must have the WordPress command line interface, WP-CLI installed.

Downloads, creates database, installs WordPress, creates virtual server for your Apache server installs WordPress, removes unneeded files, and sets folder and files permissions.

You can configure nginx for either production or local just use the appropriate php file.

The production file is configured for fastcgi caching and letsencrypt ssl.

Even if you don't use the wp-cli to install WordPress you cand use the config-install2 file to easily configure nginx.

Install Instructions:

Change the .htaccess file to fit your needs and don't forget to change example.com to your domain

You might want to change the debug and error display( in config-install1.sh) for the wp-config.php file if this is going to be for production

Feel free to change the apache or nginx config setting in the config-install2* file to fit your needs

Change the directories to where appropriate in both files

mkdir wp-cli-install/ clone project into

Run:

Bash config-wp-install1.sh

Then run:

Sudo Bash config-wp-install2-apache.sh or config-wp-install2/production/local.nginx.sh

Add your domain to your hosts file or your DNS.

If you are using windows and VirtualBox add your APP_NAME to your \windows\System32\drivers\etc\hosts file

You must change all example.com references to your domain in the .htaccess file.

